#include <iostream>
#include <SFML/Network.hpp>


void main() {
	sf::TcpListener listener;
	sf::Socket::Status status = listener.listen(50000);
	sf::TcpSocket socket;
	status = listener.accept(socket);
	if (status == sf::Socket::Done) {
		socket.send("hola", 4);
		char buffer[10];
		size_t bR;
		socket.receive(buffer, 10, bR);
		std::cout << buffer << std::endl;
	}
}